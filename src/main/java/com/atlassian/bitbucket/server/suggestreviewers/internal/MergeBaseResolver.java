package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.scm.git.GitScmConfig;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.merge.GitMergeBaseBuilder;
import com.atlassian.bitbucket.server.suggestreviewers.internal.util.GitUtils;
import com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git.FirstLineOutputHandler;

/**
 * Determines the merge base of a pair of commits.
 */
public class MergeBaseResolver {

    private final GitCommandBuilderFactory builderFactory;
    private final GitScmConfig config;
    private final CommitService commitService;

    public MergeBaseResolver(GitCommandBuilderFactory builderFactory, GitScmConfig config, CommitService commitService) {
        this.builderFactory = builderFactory;
        this.config = config;
        this.commitService = commitService;
    }

    public Commit findMergeBase(Commit a, Commit b) {
        GitMergeBaseBuilder builder = builderFactory.builder(a.getRepository())
                .mergeBase()
                .between(a.getId(), b.getId());
        GitUtils.setAlternateIfCrossRepository(builder, a.getRepository(), b.getRepository(), config);

        String sha = builder.build(new FirstLineOutputHandler()).call();
        if (sha == null) {
            return null;
        }

        return commitService.getCommit(new CommitRequest.Builder(a.getRepository(), sha).build());
    }
}
