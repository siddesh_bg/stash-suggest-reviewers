package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.user.ApplicationUser;

public class RankedUser implements Comparable<RankedUser> {

    private final ApplicationUser user;
    private int score;

    public RankedUser(ApplicationUser user) {
        this.user = user;
    }

    public ApplicationUser getUser() {
        return user;
    }

    public void add(int score) {
        this.score += score;
    }

    @Override
    public int compareTo(RankedUser other) {
        return other.score - score;
    }
}
